class Island:
    def __init__(self, definition):
        self._data = [[int(v) for v in row.split()]
                      for row in definition.split('\n') if row != '']

    def value(self, x, y):
        return self._data[x][y]

    def shape(self):
        return (len(self._data), len(self._data[0]))

    def idx_enumerate(self):
        dim1, dim2 = self.shape()
        for x in range(dim1):
            for y in range(dim2):
                yield (x, y), self.value(x, y)


def display_result(island, result):
    dim1, dim2 = island.shape()
    grid = [['-'] * dim2 for _ in range(dim1)]
    for (i, j) in result:
        grid[i][j] = '!'
    print('\n'.join(' '.join(r) for r in grid))


def _connected(graph, node, visited=None):
    visited = visited or set()
    visited.add(node)
    for n in graph[node] - visited:
        _connected(graph, n, visited)
    return visited


def solve(island):
    dim1, dim2 = island.shape()

    graph = dict(A=set(), B=set())
    for (i, j), val in island.idx_enumerate():
        ns = [(i - 1, j), (i + 1, j), (i, j - 1), (i, j + 1)]
        ns = [n for n in ns if 0 <= n[0] < dim1 and 0 <= n[1] < dim2]
        graph[i, j] = set(n for n in ns if island.value(n[0], n[1]) >= val)

        if i == 0 or j == dim2 - 1: graph['A'].add((i, j))
        if i == dim1 - 1 or j == 0: graph['B'].add((i, j))

    return _connected(graph, 'A') & _connected(graph, 'B')


ISLAND_DEF = '''
10 0 10 10 10
10 5 15 15 10
10 15 0 0  100
10 10 10 10 10
'''


def main():
    island = Island(ISLAND_DEF)
    r = solve(island)
    display_result(island, r)


if __name__ == "__main__":
    main()
